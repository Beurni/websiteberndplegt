const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSass = new ExtractTextPlugin({
    filename: "../css/main.min.css"
});

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist/scripts'),
    filename: 'main.min.js',
    publicPath: 'dist/scripts'
  },
  module: {
    rules: [{
        test: /\.scss$/,
        use: extractSass.extract({
            use: [{
                loader: "css-loader"
            }, {
                loader: "sass-loader"
            }],

            fallback: "style-loader"
        })
    }]
  },
  plugins: [
    extractSass
  ]
};