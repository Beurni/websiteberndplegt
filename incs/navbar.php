<nav class="bp-navbar navbar navbar-expand-lg">
    <div class="container">
        <a class="navbar-brand bp-navbar-title" href="index.php?page=home">Bernd Plegt</a>
        <button class="bp-navbar-toggle navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
        </button>
        <div class="collapse bp-dropdown-body navbar-collapse" id="navbarSupportedContent">
            <ul class="bp-nav-list navbar-nav ml-auto">
                <li class="bp-nav-item nav-item active">
                    <a class="bp-nav-link nav-link" href="index.php?page=home">Home</a>
                </li>
                <li class="bp-nav-item nav-item">
                    <a class="bp-nav-link nav-link" href="index.php?page=about_me">Over mij</a>
                </li>
                <li class="bp-nav-item nav-item">
                    <a class="bp-nav-link nav-link" href="index.php?page=contact">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>