<?php

    if(isset($_GET["page"])) {

        if(file_exists("./incs/" . $_GET["page"] . ".php")) {

            include("./incs/" . $_GET["page"] . ".php");

        } else {

            include("./incs/not_found.php");
        }
    } else {
        header("location: index.php?page=home");
    }
?>