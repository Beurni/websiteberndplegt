<div id="particles-js"></div>
<div class="bp-main-content bp-home-content">
    <div class="container">
        <div class="jumbotron bp-jumbotron">
            <h1 class="display-4 bp-jumbotron-title">Welkom op de website van Bernd Plegt</h1>
        </div>
        <div class="card bp-link-card">
            <button class="btn bp-read-more-btn btn-primary" onclick="window.location.href='index.php?page=about_me'">Lees meer</button>
        </div>
    </div>
</div>