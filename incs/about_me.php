<div class="bp-main-content bp-about-me-content">
    <div class="bp-about-me-section">
        <div class="container">
            <h1 class="bp-section-title">Over mij</h1>
            <div class="row">
                <div class="col-md-7 bp-about-me-content-wrapper">
                    <p>
                        Mijn naam is Bernd Plegt, ik ben 20 jaar oud en woon in Almelo. Momenteel zit ik in het laatste jaar van mijn studie ICT-beheer niveau 4, die ik volg aan het ROC van Twente. In mijn vrije tijd vind ik het leuk om te programmeren. Hier hoop ik dan in de toekomst ook mijn beroep van te kunnen maken.
                    </p>
                    <p>
                        Voor het laatste semester op school moet ik een half jaar stage lopen (van 03-09-2018 tot en met 25-01-2019). Ik ben opzoek naar een stage waarbij ik mijn programmeer vaardigheden verder kan ontwikkelen.
                    </p>
                </div>
                <div class="col-md-5">
                    <div class="bp-image-wrapper">
                        <img class="bp-image shadow-lg p-3 mb-5 bg-white" src="./img/BerndPlegt.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bp-skills-section">
        <div class="container">
            <h1 class="bp-section-title">Vaardigheden</h1>
            <div class="row">
                <div class="col-md-4">
                    <div id="html-card" class="card bp-skill-card">
                        &lt;HTML&gt;
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="css-card" class="card bp-skill-card">
                        CSS { }
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="php-card" class="card bp-skill-card">
                        &lt;?PHP
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div id="csharp-card" class="card bp-skill-card">
                        C#
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="javascript-card" class="card bp-skill-card">
                         JavaScript()
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="sql-card" class="card bp-skill-card">
                        SQL
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>