<div class="bp-contact-content">
    <div class="container">
        <h1 class="bp-page-title">Contact</h1>
        <div class="row">
            <div class="col-md-3">
                <ul class="bp-contact-list">
                    <li class="bp-contact-item"><i class="fas fa-user"></i> Bernd Plegt</li>
                    <li class="bp-contact-item"><i class="fas fa-envelope"></i> b.plegt@outlook.com</li>
                    <li class="bp-contact-item"><i class="fas fa-phone"></i> 06 45644299</li>
                </ul>
            </div>
            <div class="col-md-7">
                <form class="bp-msg-form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Voornaam</label>
                                <input type="text" class="bp-msg-input form-control" placeholder="Voornaam">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Achternaam</label>
                                <input type="text" class="bp-msg-input form-control" placeholder="Achternaam">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Email adres</label>
                        <input type="email" class="bp-msg-input form-control" placeholder="Uw email adres">
                    </div>
                    <div class="form-group">
                        <label>Bericht</label>
                        <textarea class="bp-msg-input form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Bericht"></textarea>
                    </div>
                    <button class="bp-send-msg-btn btn btn-primary">Stuur bericht</button>
                </form>
            </div>
        </div>
    </div>
</div>